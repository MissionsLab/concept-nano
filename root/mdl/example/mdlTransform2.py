from pubsub import pub

from root.mdl.base import mdlBaseModel


class Transform2 (mdlBaseModel.BaseModel):

    def __init__(self, parent = None, children = []):
        super(Transform2, self).__init__(parent=parent, children=children)
        self.undo = { self.multiplyValue: self.divideValue, self.divideValue: self.multiplyValue }
        pub.sendMessage("root.transform2.init")

    def multiplyValue(self, operator=10, uid='1', attrib='value'):

        self.dataElement(uid, attrib).value = self.dataElement(uid, attrib).value * operator
        #notice = "MONEY CHANGED"

        #now tell anyone who cares that the value has been changed
        self.log.record('action', model=self, func=self.multiplyValue, args=operator)
        #pub.sendMessage(notice, msg=self.dataElement(uid, attrib).value)

    def divideValue(self, operator=10, uid='1', attrib='value'):

        self.dataElement(uid, attrib).value = self.dataElement(uid, attrib).value / operator
        #notice = "MONEY CHANGED"

        #now tell anyone who cares that the value has been changed
        self.log.record('action', model=self, func=self.divideValue, args=operator)
        #pub.sendMessage(notice, msg=self.dataElement(uid, attrib).value)

if __name__=='__main__':
    m = Transform2()