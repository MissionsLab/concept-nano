from pubsub import pub

from root.mdl.base import mdlBaseModel


class Transform1 (mdlBaseModel.BaseModel):

    def __init__(self, parent = None, children = []):

        super(Transform1, self).__init__(parent=parent, children=children)
        self.undo = { self.addValue: self.subtractValue, self.subtractValue: self.addValue }
        pub.sendMessage("root.model2.init")

    def addValue(self, operator=0, uid='1', attrib='value'):

        self.dataElement(uid, attrib).value += operator
        #notice = "MONEY CHANGED"

        #now tell anyone who cares that the value has been changed
        self.log.record('action', model=self, func=self.addValue, args=operator)
        #pub.sendMessage(notice, msg=self.dataElement(uid, attrib).value)

    def subtractValue(self, operator=0, uid='1', attrib='value'):

        self.dataElement(uid, attrib).value -= operator
        #notice = "MONEY CHANGED"

        #now tell anyone who cares that the value has been changed
        self.log.record('action', model=self, func=self.subtractValue, args=operator)
        #pub.sendMessage(notice, msg=self.dataElement(uid, attrib).value)

if __name__=='__main__':
    m = Transform1()
