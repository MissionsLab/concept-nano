import os

from pubsub import pub

from root.mdl.base import mdlBaseModel
from root.mdl.example import mdlTransform1, mdlTransform2
from root.mdl.visualisation import mdlModel3D
from root.mdl.data import mdlData
from root.mdl.editor import mdlEditor
from root.mdl.tree import mdlTree


class MainModel (mdlBaseModel.BaseModel):
    def __init__(self):
        super(MainModel, self).__init__()

        self.model1 = mdlTransform2.Transform2(self)
        self.model2 = mdlTransform1.Transform1(self)
        self.visual = mdlModel3D.Model3D(self)
        self.editor = mdlEditor.Editor(self)
        self.tree = mdlTree.Tree(self)

        self.file = os.path.join(os.getcwd() + '\example\data', 'data.xml')
        self.mdlFileHandler = mdlData.Data(fn=self.file)
        self.data = self.mdlFileHandler.data

        # initial population of models


        pub.sendMessage("root.main.init")


    def selectVariable(self):
        if self.data['selectVar'] == 'myMoney1': self.data['selectVar'] = 'myMoney2'
        else: self.data['selectVar'] = 'myMoney1'

    def hi(self):
        print(type(self.parent))

if __name__=='__main__':
    m = MainModel()
    m.hi()