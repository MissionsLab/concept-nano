__author__ = 'SteveG'
import csv

from pubsub import pub


class Log (object):

    def __init__(self, parent=None, fn=None):
        self.records = []   # used for record
        self.actions = []   # used for undo
        self.parent = parent
        if fn is not None:
            self.load(fn)
        pub.sendMessage("root.log.init")

    def action(self, model=None, func=None, args=None):
        self.actions.append((model, func, args))

    def record(self, type, model=None, func=None, args=None, msg=None):
        # the below implies we are using old style classes...as just .__name__ should work
        if type == 'action':
            self.action(model, func, args)
            arg = [ type, model.__class__.__name__, func.__name__, args ]
        else:
            arg = [ type, msg ]
        self.records.append(arg)
        # should be possible to create decorator or something to auto generate topics?
        # pub.sendMessage("root.log." + self.record.__name__, arg=''.join((str(arg), '\n')))

    def undo(self):
        try: action = self.actions.pop()
        except: raise BufferError("Action stack is empty, cannot undo")
        if action is not []:
            action[0].undo[action[1]](action[2])
            arg = self.actions.pop()
            pub.sendMessage("root.log." + self.undo.__name__, arg=arg)

    def save(self, fn='data/example/record.log'):
        file = open(fn, 'wb')
        w = csv.writer(file, dialect='excel')
        w.writerows(self.records)
        file.close()
        pub.sendMessage("root.log.save", arg=file)

    def load(self, fn='data/example/record.log'):
        try: file = open(fn, 'rb')
        except: raise NameError("Cannot load file does not exist, new file will be created")
        if file is not None:
            r = csv.reader(file, dialect='excel')
            self.records = list(r)
            print(self.records)
            file.close()
            pub.sendMessage("root.log.load", arg=file)

if __name__=='__main__':
    m = Log()


