__author__ = 'SteveG'

from pubsub import pub

class Attribute(object):

    def __init__(self, value=0, lock=False, editor=None, link=None):
        self._value = value
        self._lock = lock
        if editor == None: self._editor = type(value)
        self._link = link
        # may have more meta here common to all attributes, some can't be changed
        pub.sendMessage("root.data.attribute.init")

    @property
    def value(self):
        pub.sendMessage("root.data.value.get", arg=self._value)
        return self._value

    @value.setter
    def value(self, value):
        if self._lock == False:
            prev = self._value
            self._value = value
            if self._link is not None:
                self.link(self._value)
                pub.sendMessage("root.data.model.update")
            pub.sendMessage("root.data.value.set", msg='', arg=self._value)
        else:
            pub.sendMessage("root.data.value.locked", arg=self._value)
        return self._value

    @property
    def lock(self):
        return self._lock

    @lock.setter
    def locked(self, lock):
        self._lock = lock
        pub.sendMessage("root.data.lock.change", arg=self._lock)

    @property
    def link(self):
        return self._link

    @link.setter
    def link(self, link):
        self._link = link
        self._link(self._value)
        pub.sendMessage("root.data.link.change", arg=self._link)

    @property
    def editor(self):
        return self._editor

    @editor.setter
    def editor(self, editor):
        pub.sendMessage("root.data.editor.locked", arg=self._editor)