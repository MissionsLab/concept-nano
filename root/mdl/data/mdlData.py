import root.mdl.base.mdlBaseModel as mdlBaseModel
from lxml import etree, objectify
from pubsub import pub
import root.mdl.visualisation.mdlVTK as mdlVTK
import root.mdl.data.mdlElement as mdlElement

class Data (mdlBaseModel.BaseModel):

    def __init__(self, fn='data\example\data.xml', parent=None):
        super(Data, self).__init__()
        #self.tree = self.loadXML(fn)
        self.objectifyXML()
        self.data = self.XMLToDict(self.tree)
        pub.sendMessage("root.data.init")

    def loadXML(self, fn='data\example\data.xml'):
        fread = open(fn, 'r')
        xmlread = fread.read()
        xmlparser = etree.XMLParser(remove_comments=True)
        xml = etree.XML(xmlread, xmlparser)
        fread.close()
        return xml

    def objectifyXML(self, sn='data\example\schema.xsd', fn='data\example\data.xml'):
        fileSchema = open(sn, 'rb')
        fileInstance = open(fn, 'rb')
        self.schema = etree.XMLSchema(etree=etree.parse(fileSchema))
        self.parser = objectify.makeparser(schema=self.schema)
        self.tree = objectify.parse(fileInstance, parser=self.parser)

    def XMLToDict(self, tree):
        data = {}
        msn = tree.findall("{*craft-data}MISSION")
        if len(msn) <> 1: raise AttributeError('XML Parsing Error: Too Many Missions')
        else: msn = msn[0]
        for ele in msn.iter():
            uid = ele.get('uid')
            data[uid] = self.addElement(ele)
            pub.sendMessage('root.data.data.XMLToDict', arg=data[uid])
        return data

    def addElement(self, ele):
        ele = mdlElement.Element(ele)
        pub.sendMessage('root.mdl.data.Data.addElement', arg=ele)
        return ele

    def getName(self, uid):
        return self.data[uid]['name'].value

