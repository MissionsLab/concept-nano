__author__ = 'SteveG'

import root.mdl.base.mdlBaseModel as mdlBaseModel
import root.mdl.data.mdlAttribute as mdlAttribute
from pubsub import pub
import root.mdl.visualisation.mdlVTK as mdlVTK

# below not currently used in favour of mdlAttribute but used to write this
class Element(mdlBaseModel.BaseModel):
    def __init__(self, ele=None, parent=None, children = []):
#        super(mdlElement, self).__init__(parent, children)

        self._name = "undefined"
        self._uid = -1
        self._class = None

        self._pid = None
        self._attribute = {}
        self._model = None

        self._children = []
        self._parent = None

        self.initElement(ele)

    def initElement(self, ele):
        if ele is not None:

            # children
            for child in ele.iterchildren():
                self._children.append(child.get('uid'))

            # critical consituents of an element
            try:
                self._uid = str(ele.get('uid'))
                self._name = str(ele.get('name'))
                self._eclass = str(ele.get('eclass')) #TODO: this should point to specific element class
            except:
                pub.sendMessage('warn.mdlElement.initElement', arg='created incomplete element')

            # create dictionary attributes of the element
            for a in ele.keys():
                if a not in ['uid', 'name', 'eclass', 'pid', 'model']:
                    if a in [ 'value', 'position.x', 'position.y', 'position.z' ]:
                    # this is a temporary hack until the attribute file is read from xml as this file contains much
                    # more than just the type but also editor metadata
                        self._attribute[a] = mdlAttribute.Attribute(float(ele.get(a)))
                    else:
                        self._attribute[a] = mdlAttribute.Attribute(ele.get(a))

            # non-critical constituents of an element
            if ele.get('pid') is not None: self._pid = ele.get('pid')
            if ele.get('model') is not None: self.initModel()

        else:
            pub.sendMessage('warn.mdlElement.initElement', arg='created unknown element')

    def initModel(self):
        # FIXME: temporary link to asmSphere class, should be multiple classes
        self._model = mdlVTK.asmSphere()
        self._attribute['position.x'].link = self.model.PartRegister['body'].setPositionX
        self._attribute['position.y'].link = self.model.PartRegister['body'].setPositionY
        self._attribute['position.z'].link = self.model.PartRegister['body'].setPositionZ
        self._attribute['value'].link = self.model.PartRegister['body'].setRadius

    @property
    def attribute(self):
        """
        This method is performed every time the myMoney property is read.
        :return:
        """
        return self._attribute

    @attribute.setter
    def attribute(self, attrib):
        """
        This method is accessed every time the myMoney property is changed.
        Here input checks and notifications can be performed.
        """
        self._attribute = attrib
        pub.sendMessage("warn.element.attribute", msg=self.value)

    @property
    def name(self):
        """
        This method is performed every time the myMoney property is read.
        :return:
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        This method is accessed every time the myMoney property is changed.
        Here input checks and notifications can be performed.
        """
        self._name = name

    @property
    def uid(self):
        """
        This method is performed every time the myMoney property is read.
        :return:
        """
        return self._uid

    @uid.setter
    def uid(self, uid):
        """
        This method is accessed every time the myMoney property is changed.
        Here input checks and notifications can be performed.
        """
        self._uid = uid

    @property
    def eclass(self):
        return self._eclass

    @eclass.setter
    def eclass(self, eclass):
        self._eclass = eclass

    @property
    def pid(self):
        """
        This method is performed every time the myMoney property is read.
        :return:
        """
        return self._pid

    @pid.setter
    def pid(self, pid):
        """
        This method is accessed every time the myMoney property is changed.
        Here input checks and notifications can be performed.
        """
        self._pid = pid

    @property
    def model(self):
        return self._model

    @model.setter
    def model(self, model):
        pub.sendMessage('warn.data.element.model', arg=("Model is read only, cannot set to %s" % model))
        print('ooops')

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, children):
        self._children = children

    @property
    def treeitem(self):
        return self._treeitem

    @treeitem.setter
    def treeitem(self, treeitem):
        self._treeitem = treeitem

#    def readXML(self, xml):
#        self.name = xml.get("name")
#        self.uid = int(xml.get("uid"))
#        self.value = float(xml.get("value"))
#        self.bid = int(xml.get("bid"))
#
#    def writeXML(self):
#        el = etree.Element(type(self).__name__, name=self.name, uid=repr(self.uid), value=repr(self.value), bid=repr(self.bid))
#        return el