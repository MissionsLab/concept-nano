import vtk

### SOURCE POLYGON DATA ###

class srcSphere(vtk.vtkTransformPolyDataFilter):

    def __init__(self, r=1, mov=(0,0,0), rot=(0,1,0,0), res=(20,40)):

        self.src = vtk.vtkSphereSource()
        self.src.SetRadius(r)
        self.src.SetPhiResolution(res[0])
        self.src.SetThetaResolution(res[1])

        self.SetTransform(tfmTranslateRotate(mov, rot))
        self.AddInputConnection(self.src.GetOutputPort())

class srcCone(vtk.vtkTransformPolyDataFilter):

    # in a cone the point is +X by default

    def __init__(self, r=1, h=3, mov=(0,0,0), rot=(0,1,0,0), res=20):

        cone = vtk.vtkConeSource()
        cone.SetRadius(r)
        cone.SetHeight(h)
        cone.SetResolution(res)

        self.SetTransform(tfmTranslateRotate(mov, rot))
        self.AddInputConnection(cone.GetOutputPort())

class srcCylinder(vtk.vtkTransformPolyDataFilter):

    def __init__(self, r=0.1, h=10, mov=(0,0,0), rot=(0,1,0,0), res=20):

        cylinder = vtk.vtkCylinderSource()
        cylinder.SetRadius(r)
        cylinder.SetHeight(h)
        cylinder.SetResolution(res)

        self.SetTransform(tfmTranslateRotate(mov, rot))
        self.AddInputConnection(cylinder.GetOutputPort())

class srcCuboid(vtk.vtkTransformPolyDataFilter):

    def __init__(self, x=1, y=1, z=1, mov=(0,0,0), rot=(0,1,0,0)):

        cuboid = vtk.vtkCubeSource()
        cuboid.SetXLength(x)
        cuboid.SetYLength(y)
        cuboid.SetZLength(z)

        self.SetTransform(tfmTranslateRotate(mov, rot))
        self.AddInputConnection(cuboid.GetOutputPort())

### DEFAULT TRANSFORM ###

class tfmTranslateRotate(vtk.vtkTransform):

    def __init__(self, mov, rot):
        self.Translate(mov[0], mov[1], mov[2])
        self.RotateWXYZ(rot[0], rot[1], rot[2], rot[3])


### PIPELINE FUNCTIONS ###

def defaultActorPipe(act, polydata, mov, rot, rgb, alpha):

    # collate source polygons
    src = vtk.vtkAppendPolyData()
    for poly in polydata:
        src.AddInputConnection(poly.GetOutputPort())

    # pipe source polygon to transform
    tff = vtk.vtkTransformPolyDataFilter()
    tff.SetTransform(tfmTranslateRotate(mov, rot))
    tff.SetInputConnection(src.GetOutputPort())

    # pipe transform to mapper
    map = vtk.vtkPolyDataMapper()
    map.SetInputConnection(tff.GetOutputPort())

    # pipe mapper to actor
    act.SetMapper(map)

    # add colour properties to actor
    act.GetProperty().SetColor(rgb[0], rgb[1], rgb[2])
    act.GetProperty().SetOpacity(alpha)

    return tff

def defaultAssemblyPipe(act, mov, rot, scale, offset=(0,0,0)):

    act.SetOrigin(offset[0], offset[1], offset[2])
    act.SetPosition(mov[0], mov[1], mov[2])
    act.RotateWXYZ(rot[0], rot[1], rot[2], rot[3])
    act.SetScale(scale, scale, scale)

def invertAssemblyPipe(act, mov, rot, scale):

    act.SetPosition(mov[0], mov[1], mov[2])
    act.RotateWXYZ(rot[0], rot[1], rot[2], rot[3])
    act.SetScale(scale, scale, scale)

### ACTOR CLASSES ###

class actCylinder(vtk.vtkActor):

    def __init__(self, r=0.1, h=10, mov=(0,0,0), rot=(0,1,0,0), rgb=(1,0,0), alpha=1):

        polydata = []
        polydata.append(srcCylinder(r, h, (0,0,0), (0,1,0,0), 20))

        defaultActorPipe(self, polydata, mov, rot, rgb, alpha)

class actCuboid(vtk.vtkActor):

    def __init__(self, x=1, y=1, z=1, mov=(0,0,0), rot=(0,1,0,0), rgb=(1,0,0), alpha=1):

        # create list of polygons for actor
        polydata = []
        polydata.append(srcCuboid(x, y, z, (0,0,0), (0,1,0,0)))

        # create actor using default process
        defaultActorPipe(self, polydata, mov, rot, rgb, alpha)

class actSphere(vtk.vtkActor):

    def __init__(self, r=1, mov=[0,0,0], rot=[0,1,0,0], rgb=[1,0,0], alpha=1, res=[20,40]):

        self.r = r
        self.mov = mov
        self.rot = rot
        self.rgb = rgb
        self.alpha = alpha

        self.polydata = []
        self.sphere = srcSphere(self.r, self.mov, self.rot, res)
        self.polydata.append(self.sphere)

        # create actor using default process
        self.pipe = defaultActorPipe(self, self.polydata, mov, rot, rgb, alpha)

    def setRadius(self, r):
        self.r = r
        self.sphere.src.SetRadius(r)
        self.update()

    def setPositionX(self, x):
        self.mov[0] = x
        self.pipe.SetTransform(tfmTranslateRotate(self.mov, self.rot))
        self.update()

    def setPositionY(self, y):
        self.mov[1] = y
        self.pipe.SetTransform(tfmTranslateRotate(self.mov, self.rot))
        self.update()

    def setPositionZ(self, z):
        self.mov[2] = z
        self.pipe.SetTransform(tfmTranslateRotate(self.mov, self.rot))
        self.update()

    def update(self):
        pass

class actArrow(vtk.vtkActor):

    # centre is base of arrow

    def __init__(self, r0=0.1, l0=5, r1=1, l1=3, mov=(0,0,0), rot=(0,1,0,0), rgb=(1,0,0), alpha=1, res=20):

        # create list of polygons for actor
        polydata = []
        polydata.append(srcCylinder(r0, l0, (l0/2, 0, 0), (90,0,0,1), res))
        polydata.append(srcCone(r1, l1, (l0, 0, 0), (0,1,0,0), res))

        # create actor using default process
        defaultActorPipe(self, polydata, mov, rot, rgb, alpha)

### ASSEMBLY CLASSES ###

class asm(vtk.vtkAssembly):

    def __init__(self):

        self.PartRegister = {}

    def AssemblePart(self, shortname, part, parent=-1):

#        print('assembly')

        if shortname in self.PartRegister.keys():
            print('Error : part already registered')
            return

        self.PartRegister[shortname] = part

        if parent != -1:
#            print(parent)
            self.PartRegister[parent].AddPart = part
        else:
#            print('here')
            self.AddPart(part)

class asmXYZArrowSet(vtk.vtkAssembly):

    def __init__(self, mov=(0,0,0), rot=(0,1,0,0), scale=1, colour='RGB', alpha=1):

        if colour == 'RGB':
            x = (1,0,0)
            y = (0,1,0)
            z = (0,0,1)
        elif colour == 'MYC':
            x = (1,0,1)
            y = (1,1,0)
            z = (0,1,1)
        else:
            x = (1,0,0)
            y = (0,1,0)
            z = (0,0,1)

        self.AddPart(actArrow(0.1, 5, 0.3, 1, (0,0,0), (0,1,0,0), x, alpha))
        self.AddPart(actArrow(0.1, 5, 0.3, 1, (0,0,0), (90,0,0,1), y, alpha))
        self.AddPart(actArrow(0.1, 5, 0.3, 1, (0,0,0), (90,0,-1,0), z, alpha))
        self.AddPart(actSphere(0.3, (0,0,0), (0,1,0,0), (1,1,1)))

        defaultAssemblyPipe(self, mov, rot, scale)

class asmReference(vtk.vtkAssembly):

    def __init__(self):

        self.AddPart(actSphere(0.3,(0,0,0), (0,1,0,0),(1,1,1)))
        self.AddPart(actSphere(0.3,(10,0,0),(0,1,0,0),(1,0,0)))
        self.AddPart(actSphere(0.3,(0,10,0),(0,1,0,0),(0,1,0)))
        self.AddPart(actSphere(0.3,(0,0,10),(0,1,0,0),(0,0,1)))

class asmSphere(asm):

    def __init__(self, r=1, pos=[0,0,0], col=[1,1,1]):

        self.PartRegister = {}

        self.AssemblePart('body', actSphere(r, pos, [0,1,0,0], col))


class asmSatelliteBase(asm):

    def __init__(self):

        self.PartRegister = {}

        sz = (2, 1, 3)
        t = 0.0016
        d = 0.1

        self.AssemblePart('STR', actCuboid(sz[0], sz[1], sz[2], (0,0,0), (0,1,0,0), (0.5,0.5,0.5), 0.5))

        self.AssemblePart('SA-FXP', asmFixedPanel(face='XP', size=sz, t=t, d=d, rgb=(1,0,0), alpha=1, centre=True, deploys=False, hinge='R', angle=90, scale=0.1))
        self.AssemblePart('SA-FXN', asmFixedPanel(face='YP', size=sz, t=t, d=d, rgb=(0,1,0), alpha=1, centre=True, deploys=False, double=True, hinge='L', angle=90, scale=0.1))
        self.AssemblePart('SA-FYP', asmFixedPanel(face='ZP', size=sz, t=t, d=d, rgb=(0,0,1), alpha=1, centre=True, deploys=False, hinge='L', angle=90, scale=0.1))
        self.AssemblePart('SA-FYN', asmFixedPanel(face='XN', size=sz, t=t, d=d, rgb=(1,1,0), alpha=1, centre=True, deploys=False, hinge='D', angle=45, scale=0.1))
        self.AssemblePart('SA-FZP', asmFixedPanel(face='YN', size=sz, t=t, d=d, rgb=(0,1,1), alpha=1, centre=True, deploys=True, double=True, hinge='R', angle=90, scale=0.1))
        self.AssemblePart('SA-FZN', asmFixedPanel(face='ZN', size=sz, t=t, d=d, rgb=(1,0,1), alpha=1, centre=True, scale=0.1))

class asmFixedPanel(vtk.vtkAssembly):

    def __init__(self, face='XP', size=(1,1,3), t=0.0016, d=0.1, rgb=(1,0,0), alpha=1, deploys=False, hinge='U', centre=True, double=True, angle=90, scale=1):

        x, y, z = calcPanelDimensions(size=size, face=face, t=t, d=d)
        mov, rot = calcPanelTransform(size=size, face=face)

        self.AddPart(actCuboid(x, y, t, (0,0,0), (0,1,0,0), rgb, alpha))

        if centre == True:
            self.AddPart(asmXYZArrowSet((0,0,0), (0,1,0,0), 0.1))

        if deploys == True:
            self.AddPart(asmDeployablePanel(size, face, hinge, 0.001, 0.1, rgb, alpha, angle=angle, centre=True, double=double, scale=0.1))

        defaultAssemblyPipe(self, mov, rot, 1)

class asmDeployablePanel2(vtk.vtkAssembly):

    def __init__(self, size=(1,1,3), face='XP', hinge='U', t=0.0016, d=0.1, rgb=(1,0,0), alpha=1, angle=90, centre=True, scale=1):

        x, y, z = calcPanelDimensions(size=size, face=face, t=t, d=d)
       # mov, rot = calcPanelTransform(size=size, face=parent)

       # self.container = asmContainerTransform(mov=mov, rot=rot)

        if hinge == 'U':
            dx = 0
            dy = y / 2
            sign = -1
        elif hinge == 'D':
            dx = 0
            dy = -y / 2
            sign = 1
        elif hinge == 'R':
            dx = x / 2
            dy = 0
            sign = 1
        elif hinge == 'L':
            dx = -x / 2
            dy = 0
            sign = -1

        dz = 0

        if hinge in ('U', 'D'):
            self.DeployRotate = self.RotateX
        elif hinge in ('R', 'L'):
            self.DeployRotate = self.RotateY

        self.AddPart(actCuboid(x, y, t, (0,0,0), (0,1,0,0), rgb, alpha))
        self.AddPart(actCylinder(0.01, 1, (dx, dy, dz), (0,1,0,0), rgb, alpha))

        if centre == True:
            self.AddPart(asmXYZArrowSet((0,0,0), (0,1,0,0), 0.1))

        defaultAssemblyPipe(self, (0,0,0), (0,1,0,0), 1, offset=(dx, dy, dz))

        self.DeployRotate(angle*sign)


class asmContainerTransform(vtk.vtkAssembly):

    def __init__(self, mov=(0,0,0), rot=(0,1,0,0), offset=(0,0,0), scale=1):

        defaultAssemblyPipe(self, mov=mov, rot=rot, offset=offset, scale=scale)

class asmDeployablePanel(vtk.vtkAssembly):

    def __init__(self, sz, parent, hinge, t, d, rgb, alpha, angle=90, centre=True, double=True, scale=1):

        x, y, z = calcPanelDimensions(sz, parent, t, d)
        mov, rot = calcPanelTransform(sz, parent)

        if hinge == 'U':
            dx = 0
            dy = y / 2
            sign = -1
        elif hinge == 'D':
            dx = 0
            dy = -y / 2
            sign = 1
        elif hinge == 'R':
            dx = x / 2
            dy = 0
            sign = 1
        elif hinge == 'L':
            dx = -x / 2
            dy = 0
            sign = -1

        dz = 0

        if hinge in ('U', 'D'):
            self.DeployRotate = self.RotateX
        elif hinge in ('R', 'L'):
            self.DeployRotate = self.RotateY

        self.AddPart(actCuboid(x, y, t, (0,0,0), (0,1,0,0), rgb, alpha))
        self.AddPart(actCylinder(0.01, 1, (dx, dy, dz), (0,1,0,0), rgb, alpha))

        if centre == True:
            self.AddPart(asmXYZArrowSet((0,0,0), (0,1,0,0), 0.1))

        if double == True:
            self.AddPart(actCuboid(x, y, t, (x*-sign,0,0), (0,1,0,0), rgb, alpha))

        defaultAssemblyPipe(self, (0,0,0), (0,1,0,0), 1, offset=(dx, dy, dz))

        self.DeployRotate(angle*sign)

### ASSEMBLY ROUTINES ###

def calcPanelDimensions(size=(1,1,3), face='XP', t=0.0016, d=0.008):

    if face in ('XP', 'XN'):
        x = size[1] - 2*d
        y = size[2]
    elif face in ('YP', 'YN'):
        x = size[0] - 2*d
        y = size[2]
    elif face in ('ZP', 'ZN'):
        x = size[0] - 2*d
        y = size[1] - 2*d
    else:
        pass

    z = t

    return x, y, z

def calcPanelTransform(size=(1,1,3), face='XP'):

    # reference frames defined as XY in the face plane
    # +z points out from the face
    # +y aligns to +Z on XY faces
    # +x aligns with +x on the Z faces

    mov = (0,0,0)
    rot = (0,1,0,0)

    if face == 'XP' :
        mov = (size[0]/2,0,0)
        rot = (120,0.57735,0.57735,0.57735)
    elif face == 'YP':
        mov = (0,size[1]/2,0)
        rot = (180,0,0.70711,0.70711) ## THIS ONE IS WRONG
    elif face == 'ZP':
        mov = (0,0,size[2]/2)
        rot = (0,1,0,0)
    elif face == 'XN':
        mov = (-size[0]/2,0,0)
        rot = (120,0.57735,-0.57735,-0.57735)
    elif face == 'YN':
        mov = (0,-size[1]/2,0)
        rot = (-90,-1,0,0)
    elif face == 'ZN':
        mov = (0,0,-size[2]/2)
        rot = (180,1,0,0)

    return mov, rot