from pubsub import pub

import root.mdl.visualisation.mdlVTK as mdlVTK
from root.mdl.base import mdlBaseModel
from root.mdl.timer import mdlTimer


class Model3D(mdlBaseModel.BaseModel):

    def __init__(self, parent=None, children=None):
        super(Model3D, self).__init__(parent=parent, children=children)
        self.refreshTimer = mdlTimer.Timer(self, period=1, msg="root.mdl.visualisation.Model3D.refresh")
        self.refreshTimer.go()
        pub.sendMessage("root.3DModel.init")

    def actClear(self):
        self.actors = []
        pub.sendMessage("root.3DModel.actClear")

    def actPopulate(self):
        self.actors = []
        self.actors.append(mdlVTK.asmReference())
        self.actors.append(mdlVTK.asmXYZArrowSet(alpha=1))
        for uid in ['1', '2', '3', '4', '5']:
            self.actors.append(self.eleModel(uid))
        pub.sendMessage("root.3DModel.actPopulate", arg = self.actors)
