__author__ = 'SteveG'

from pubsub import pub
from root.mdl.base import mdlBaseModel


class Editor(mdlBaseModel.BaseModel):

    def __init__(self, parent=None, children=None):
        super(Editor, self).__init__(parent=parent, children=children)
        pub.sendMessage("root.mdl.editor.Editor.init")

    def actPopulate(self):
        data = self.getData()
        pub.sendMessage("root.mdl.editor.Editor.actPopulate", arg=data, msg=0)
