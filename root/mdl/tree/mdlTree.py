__author__ = 'SteveG'

from pubsub import pub
from root.mdl.base import mdlBaseModel

class Tree(mdlBaseModel.BaseModel):

    def __init__(self, parent=None, children=None):
        super(Tree, self).__init__(parent=parent, children=children)
        pub.sendMessage("root.mdl.tree.Tree.init")

    def actPopulate(self):
        data = self.getData()
        pub.sendMessage("root.mdl.tree.Tree.actPopulate", arg=data)
