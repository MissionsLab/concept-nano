import threading
from pubsub import pub
import random

class Timer(threading.Thread):

    def __init__(self, parent=None, period=10, msg='root.timer.majorCycle', func=None):
        threading.Thread.__init__(self)
        self.event = threading.Event()
        self.parent = parent
        self.period = period
        self.message = msg
        self.func = func

    def __del__(self):
        self.stop()

    def go(self):
        self.start()

    def run(self):
        while not self.event.is_set():
            self.majorCycle()
            self.event.wait(self.period)

    def hold(self):
        self.event.set()

    # major cycle defined wrt refresh rate on gui elements
    # hacked with a random number, there should be something between the timer and the view to generate the data
    def majorCycle(self):
        if self.func is not None:
            self.func()
        pub.sendMessage(self.message, arg=random.random())

if __name__=='__main__':
    m = Timer()