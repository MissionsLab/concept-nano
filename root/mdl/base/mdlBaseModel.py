import root.mdl.log.mdlLog as mdlLog

class BaseModel (object):
    def __init__ (self, parent = None, children = [], log =None):
        self.parent = parent
        self.children = children
#        self.topic = "root." + self.__name__
        if parent is None:
            self.data = dict()
            self.log = mdlLog.Log(self)
        else:
            self.log = self.parent.log
            self.data = self.parent.data
            self.parent.children.append(self)

    def dataElement(self, uid, attrib=None):
        if attrib is None:
            return self.getData()[uid]
        else:
            return self.getData()[uid].attribute[attrib]

    def eleModel(self, uid):
        return self.getData()[uid].model

    def getData (self):
        if self.parent is None:
            try:
                return self.data
            except AttributeError:
                #some logging?
                return None
        else:
            return self.parent.getData()
            # raise AttributeError('parent is missing.')
    # def setData (self):

    def readXML(self, xml):
        raise NotImplementedError("Error: this method shall be implemented in subclasses.")

    def writeXML(self):
        raise NotImplementedError("Error: this method shall be implemented in subclasses.")

if __name__=='__main__':
    m = BaseModel()