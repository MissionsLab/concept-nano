import wx

class DataView(wx.Frame):

    def __init__(self, parent):

        wx.Frame.__init__(self, parent, title="data View")

        sizer = wx.BoxSizer(wx.VERTICAL)
        txtLabel = wx.StaticText(self, label="data Variables")

        file = wx.Button(self, label='Load data')

        self.btnSelect = wx.Button(self, label='Toggle Var')
        self.txtPeriodic = wx.TextCtrl(self)

        self.szrVariables = wx.BoxSizer(wx.HORIZONTAL)
        self.populate(5)

        sizer.Add(file, 0, wx.EXPAND | wx.ALL)
        sizer.Add(txtLabel, 0, wx.EXPAND | wx.ALL)
        sizer.Add(self.szrVariables, 0, wx.EXPAND | wx.ALL)
        sizer.Add(self.txtPeriodic, 0, wx.EXPAND | wx.ALL)
        sizer.Add(self.btnSelect, 0, wx.EXPAND | wx.ALL)

        self.SetSizer(sizer)

    def setPeriodic(self, value):
        self.txtPeriodic.SetValue(str(value))

    def setValue(self, i, value):
        self.moneyCtrl[i].SetValue(str(value))

    def setLabel(self, i, label):
        self.moneyCtrl[i].SetLabel(str(label))

    def setSelect(self, i):
        self.moneyCtrl[i].SetBackground((255,0,0))

    def setUnselect(self, i):
        self.moneyCtrl[i].SetBackground((255,255,255))

    def depopulate(self):
        for i in range(0, len(self.moneyCtrl)):
            self.moneyCtrl[i].Destroy()
        self.moneyCtrl = []

    def populate(self, n):
        try: self.depopulate()
        except: self.moneyCtrl = []
        for i in range(0, n):
            self.moneyCtrl.append(wx.TextCtrl(self))
            self.szrVariables.Add(self.moneyCtrl[i])

if __name__ == '__main__':
    class MyApp(wx.App):
        def OnInit(self):
            frame = DataView(None)
            frame.Show(True)
            return True

    app = MyApp(0)