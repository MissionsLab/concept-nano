import wx
import vtk
from vtk.wx.wxVTKRenderWindowInteractor import wxVTKRenderWindowInteractor

class Model3DView(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, title="3D Model",size=(650,600), style=wx.MINIMIZE_BOX|wx.SYSTEM_MENU|
                  wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN)
        self.panel = Model3DPanel(self)
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer.Add(self.panel, 0, wx.EXPAND | wx.ALL)
        self.SetSizerAndFit(self.sizer)

class Model3DPanel(wx.Panel):

    def __init__(self, parent):

        wx.Panel.__init__(self, parent)
        self.graphic = wxVTKRenderWindowInteractor(self, -1)
        self.graphic.Enable(1)
        self.graphic.AddObserver("ExitEvent", lambda o, e, f=self: f.close())
        self.szr = wx.BoxSizer(wx.VERTICAL)
        self.szr.Add(self.graphic, 1, wx.EXPAND)
        self.SetSizerAndFit(self.szr)

        self.actRender()

        self.SetMinSize((400,400))

    def actRender(self):

        self.renderer = vtk.vtkRenderer()
        self.graphic.GetRenderWindow().AddRenderer(self.renderer)
        self.renderer.SetBackground(0.90, 0.90, 0.90)

        self.axes = vtk.vtkAxesActor()

        self.marker = vtk.vtkOrientationMarkerWidget()
        self.marker.SetInteractor(self.graphic._Iren)
        self.marker.SetOrientationMarker(self.axes)
        self.marker.SetViewport(0.75, 0.00, 1.00, 0.25)

        self.camera = vtk.vtkCamera()
        self.camera.SetPosition(-15, -15, -15)
        self.camera.SetViewUp(0, 0, 1)

        self.renderer.SetActiveCamera(self.camera)
        self.renderer.GetActiveCamera().Zoom(1)

    def actRerender(self):
        self.graphic.Update()
        self.graphic.Refresh()

    # TODO : needs a link to the data set to implement
    def actRefresh(self):
        self.renderer.Clear()
        self.actPopulate()

    def actPopulate(self, actors):
        for a in actors:
            self.renderer.AddActor(a)
        self.actRerender()

