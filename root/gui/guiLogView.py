import wx

class LogView(wx.Frame):

    def __init__(self, parent):

        wx.Frame.__init__(self, parent, title="Log")

        self.txtLog = wx.TextCtrl(self, style= wx.TE_MULTILINE, size=(800,100))
        self.btnUndo = wx.Button(self, label="Undo")
        self.btnSave = wx.Button(self, label="Save")
        self.btnLoad = wx.Button(self, label="Load")

        self.szr1 = wx.BoxSizer(wx.VERTICAL)
        self.szr2 = wx.BoxSizer(wx.HORIZONTAL)

        self.szr1.Add(self.txtLog, 0, wx.EXPAND | wx.ALL)
        self.szr2.Add(self.btnUndo, 0, wx.EXPAND | wx.ALL)
        self.szr2.Add(self.btnSave, 0, wx.EXPAND | wx.ALL)
        self.szr2.Add(self.btnLoad, 0, wx.EXPAND | wx.ALL)
        self.szr1.Add(self.szr2, 0, wx.EXPAND | wx.ALL)

        self.SetSizerAndFit(self.szr1)

    def addRecord(self, record):
        self.txtLog.WriteText(record)

    def clearRecord(self):
        self.txtLog.Clear()

if __name__ == '__main__':
    class MyApp(wx.App):
        def OnInit(self):
            frame = LogView(None)
            frame.Show(True)
            return True

    app = MyApp(0)
