__author__ = 'SteveG'

import wx
import wx.propgrid as wxpg

class FrameEditor(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent)
        self.pnlEditor = EditorPanel(self)

class EditorPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.editor = wxpg.PropertyGridManager(self)
        self.szr = wx.BoxSizer(wx.VERTICAL)
        self.szr.Add(self.editor, 0, wx.EXPAND | wx.ALL)
        self.SetSizerAndFit(self.szr)

    def addPage(self, title):
        self.editor.AddPage(title)

    def addCategory(self, ref, title):
        self.editor.Append(wxpg.PropertyCategory(title, ref))

    def addProperty(self, ref, title, value, editor, lock):
        if editor == float:
            self.editor.Append(wxpg.FloatProperty(title, ref, value=value))
        else:
            self.editor.Append(wxpg.StringProperty(title, ref, value=str(value)))
        #self.editor.EnableProperty(''.join(('1.',ref)), lock)

    def refresh(self):
        self.Layout()
       # def




# class Editor(wxpg.PropertyGridManager):
#
#     def __init__(self, parent, style='base'):
#
#         if style == 'base':
#             wxpg.PropertyGridManager.__init__(self, parent, style=wxpg.PG_TOOLBAR)
#         else:
#             wxpg.PropertyGridManager.__init__(self, parent)
#
#         self.SetMinSize((200,200))
#
#     def AddCategory(self, ref):
#         self.Append(wxpg.PropertyCategory(title, ref))
#
#     def AddProperty(self, ref, value, editor):
#


#
#
#         # TODO : 1 should load this once not everytime called
#         self.AttributeXML, self.AttributeParser = ReadOnlyXML(os.path.join('xml','attribute.xml'))
#
#         self.EditorList = []
#
#         # NOTE: Editor must be registered *before* adding a property that
#         # uses it.
#         if not getattr(sys, '_PropGridEditorsRegistered', False):
#
#             # test editors
#             #self.RegisterEditor(TrivialPropertyEditor)
#             #self.RegisterEditor(SampleMultiButtonEditor)
#             #self.RegisterEditor(LargeImageEditor)
#
#             # actual editors
#             self.RegisterEditor(ArrayConfigurationEdit(self.Log))
#             self.EditorList.append("ArrayConfigurationEdit")
#
#             # for test purposes
#             self.RegisterEditor(TrivialPropertyEditor)
#             self.EditorList.append("TrivialPropertyEditor")
#
#             self.RegisterEditor(CustomPropertyEditor)
#             self.EditorList.append("Custom")
#
#             #self.RegisterEditor(SingleChoiceDialogAdapter)
#             #self.EditorList.append("SingleChoiceDialogAdapter")
#
#             # ensure we only do it once
#             sys._PropGridEditorsRegistered = True
#
#         # Show help as tooltips
#         self.SetExtraStyle(wxpg.PG_EX_HELP_AS_TOOLTIPS)
#
#         # Define callbacks
#         self.Bind(wxpg.EVT_PG_CHANGED, self.cbParameterChange)
#         self.Bind(wxpg.EVT_PG_PAGE_CHANGED, self.cbPageChange)
#         self.Bind(wxpg.EVT_PG_SELECTED, self.cbParameterSelect)
#         self.Bind(wxpg.EVT_PG_RIGHT_CLICK, self.cbParameterLock)
#
#         # initialise the last changed records
#         self.LastChanged = 0
#         self.LastChangedParent = 0
#
#         self.Populate()
#
#     def Populate(self):
#
#         # create mission page
#         msn = self.GetTitle(dataElementDict.data.tree)
#         self.AddPage(msn, self.GetIcon(dataElementDict.data.tree))
#         self.Append(wxpg.PropertyCategory(msn))
#         self.AddAttributes(dataElementDict.data.tree)
#
#         #print(self.EditorList)
#
#         # create sub-mission pages
#         for ele in dataElementDict.data.tree.iterchildren():
#             title = self.GetTitle(ele)
#             print(ele.tag)
#             if self.style == 'base':
#                 if (ele.tag != 'Global'):
#                     try:
#                         self.AddPage(title, self.GetIcon(ele.tag))
#                     except:
#                         print(title)
#                         self.AddPage(title)
#                     self.SelectPage(title)
#                 else:
#                     self.SelectPage('MISSION')
#             self.Append(wxpg.PropertyCategory(title))
#             self.AddAttributes(ele)
#             self.IterateCategory(ele, 1)
#
#         # collapse all data
#         self.CollapseAll()
#
#         # show mission page on start
#         self.SelectPage(msn)
#         self.Expand(msn)
#
#     def ParseOrbit(self, data):
#         self.Log.timewrite('Parsing all orbits...\n')
#         for ele in data.iterchildren():
#             if ele.tag == 'Constellation':
#                 for orb in ele.iterchildren():
#                     orbit = {}
#
#                     if orb.tag == 'Orbit':
#                         self.Log.timewrite('    ...working on %s\n' % orb.get('Name'))
#
#                         # create orbital model
#                         for key in orb.keys():
#                             orbit[key] = float(orb.get(key))
#
#                         if orb.get('OrbitType') == 'Circular':
#                             orbit('ra', calc_ra(orb.get('za')))
#                             orb.set('rp', calc_rp(orb.get('zp')))
#                             orb.set('a', calc_a(orb.get('zp'), orb.get('za')))
#                             orb.set('e', calc_e(orb.get('zp'), orb.get('za')))
#                             orb.set('i', deg2rad(orb.get('ideg')))
#                             orb.set('n', calc_n(orb.get('a')))
#                             orb.set('T', calc_T(orb.get('T')))
#                             orb.set('Start.MJD', orb.get('Start.Date').GetMJD() + orb.get('Start.Time') / 86400)
#                             orb.set('End.MJD', orb.get('Start.Date').GetMJD() + orb.get('Start.Time') / 86400)
#                             orb.set('DT', (orb.get('End.MJD') - orb.get('Start.MJD')) * 86400)
#                             orb.set('N', calc_N(orb.get('DT'), orb.get('T')))
#                         else:
#                             orb.set('ra', calc_ra(orb.get('za')))
#                             orb.set('rp', calc_rp(orb.get('zp')))
#                             orb.set('a', calc_a(orb.get('zp'), orb.get('za')))
#                             orb.set('e', calc_e(orb.get('zp'), orb.get('za')))
#                             orb.set('i', deg2rad(orb.get('ideg')))
#                             orb.set('n', calc_n(orb.get('a')))
#                             orb.set('T', calc_T(orb.get('T')))
#                             orb.set('Start.MJD', orb.get('Start.Date').GetMJD() + orb.get('Start.Time') / 86400)
#                             orb.set('End.MJD', orb.get('Start.Date').GetMJD() + orb.get('Start.Time') / 86400)
#                             orb.set('DT', (orb.get('End.MJD') - orb.get('Start.MJD')) * 86400)
#                             orb.set('N', calc_N(orb.get('DT'), orb.get('T')))
#         self.Log.timewrite('    ...complete\n')
#
#     def IterateCategory(self, data, level):
#         for ele in data.iterchildren():
#             self.AddCategory(ele, level)
#             self.IterateCategory(ele, level+1)
#
#     def AddCategory(self, ele, level):
#         if level == 1:
#             title = ele.get('Name')#'--- ' + str.upper(ele.get('name')) + ' ---'
#         else:
#             title = ele.get('Name')
#         ref = ele.get('UID') + '.Category'
#         cat = self.Append(wxpg.PropertyCategory(title, ref))
#
#         #FIXME: 3 these settings for categories do not work, different command?
#         self.SetPropertyBackgroundColour(cat, 'BLUE')
#         self.SetPropertyTextColour(cat, 'BLACK')
#
#         self.AddAttributes(ele)
#
#     def AddProperty(self, attrib, ref, value, longref, editor, name, variable):
#         # TODO : 2 merge this search with siilar functionality performed in dict
#         # TODO : 2 consider making the editor based on a type assignment within dict
#         try:
#             # look for specific first, if not look for generic
#             variable = self.AttributeXML.xpath(".//Attribute[@Ref='" + longref + "']")[0]
#             editor = variable.get('Editor')
#             name = variable.get('Name')
#         except:
#             # look for generic
#             try:
#                 root = longref.split('.')[0]
#                 #print(root)
#                 variable = self.AttributeXML.xpath(".//Attribute[@Ref='" + root + "']")[0]
#                 editor = variable.get('Editor')
#                 name = variable.get('Name')
#             except:
#                 pass
#             editor = 'string'
#             name = attrib + ' [No Ref]'
#         # evaluate type
#         if editor == 'string':
#             prop = wxpg.StringProperty(name, ref, value=value)
#         if editor == 'date':
#             # TODO : 3 figure out why the high level definition of MJD doesn't trigger
#             # TODO : 2 display time as well as date in editor and ensure that DD/MM/YY HH:MM:SS.mmmm format is used
#             temp = wx.DateTime()
#             temp.SetJDN(float(value))
#             #temp.SetFormat()
#             prop = wxpg.DateProperty(name, ref, value=temp)
#             ##prop = wxpg.TimeProperty(name, ref + '2', value=temp.GetMinute())
#         elif editor == 'array':
#             prop = wxpg.ArrayStringProperty(name, ref, value=value.split(', '))
#         elif editor == 'float':
#             try:
#                 prop = wxpg.FloatProperty(name, ref, value=float(value))
#             except:
#                 prop = wxpg.StringProperty(name, ref, value=value)
#         elif editor == 'normal':
#             prop = wxpg.FloatProperty(name, ref, value=float(value))
#         elif editor == 'bool':
#             prop = wxpg.BoolProperty(name, ref, value=bool(value))
#         elif editor == 'enum':
#             try:
#                 ele = self.AttributeXML.xpath(".//Enum[@Ref='" + variable.get('Ref') + "']")[0]
#                 list = str.split(ele.get('Values'), ', ')
#             except:
#                 self.Log.timewrite('Cannot load enumerated list for %s \n' % variable.get('Name'))
#                 list = 'error'
#                 idx = 0
#             try:
#                 idx = list.index(value)
#                 prop =  wxpg.EnumProperty(name, ref, list, range(0, len(list)), idx)
#             except:
#                 self.Log.timewrite('Cannot index %s in %s for %s\n' % (value, variable.get('Name'), name))
#                 prop = wxpg.StringProperty(name, ref, value=str([value + 'enum error']))
#         elif editor == 'colour':
#             col = wx.Colour()
#             try: col.SetFromName(value)
#             except: col.Set(255, 0, 0)
#             prop = wxpg.ColourProperty(name, ref, value=col.Get())
#         elif editor == 'int':
#             prop = wxpg.IntProperty(name, ref, value=int(value))
#         else:
#             prop = wxpg.StringProperty(name, ref, value=value)
#
#         prop.editor = editor
#         return prop
#
#     def AddAttributes(self, ele):
#         sub = {}
#         for attrib in sorted(ele.attrib):
#             if attrib not in attribhide:
#                 try:
#                     variable = self.AttributeXML.xpath(".//Attribute[@Ref='" + attrib + "']")[0]
#                     editor = variable.get('Editor')
#                     name = variable.get('Name')
#                 except:
#                     variable = 'error'
#                     editor = 'string'
#                     name = attrib + ' (attribute not in database!)'
#                 if '.' in attrib:
#                     body = attrib.split('.')
#                     lead = ele.get('UID') + '.' + body[0]
#                     if lead in sub.keys():
#                         prop = self.AppendIn(sub[lead], self.AddProperty(body[1], body[1], ele.get(attrib), attrib, editor, name, variable))
#                         self.SetPropertyBackgroundColour(prop, 'WHITE')
#                         self.SetPropertyTextColour(prop, 'DARK BLUE')
#                         self.EnableProperty(prop, True)
#                     else:
#
#                         sub[lead] = self.Append(wxpg.StringProperty(body[0], lead, "<composed>"))
#                         self.EnableProperty(sub[lead], False)
#                         self.SetPropertyBackgroundColour(sub[lead], 'LIGHT GREY')
#                         self.SetPropertyTextColour(sub[lead], 'GREY')
#                         prop = self.AppendIn(sub[lead], self.AddProperty(body[1], body[1], ele.get(attrib), attrib, editor, name, variable))
#                         self.SetPropertyBackgroundColour(prop, 'WHITE')
#                         self.SetPropertyTextColour(prop, 'DARK BLUE')
#                         self.EnableProperty(prop, True)
#                 else:
#                     prop = self.Append(self.AddProperty(attrib, ele.get('UID') + '.' + attrib, ele.get(attrib), attrib, editor, name, variable))
#                     self.SetPropertyBackgroundColour(prop, 'WHITE')
#                     self.SetPropertyTextColour(prop, 'DARK BLUE')
#                     self.EnableProperty(prop, True)
#
#                 # if custom property editor
#                 if editor in self.EditorList:
#                     self.SetPropertyEditor(prop, "CustomPropertyEditor")
#                 else:
#                     prop.editor = 'default'
#
#
#     def GetTitle(self, ele):
#         title = ele.get('Name')
#         if title == None:
#             title = ele.tag
#         return title# + ' [' + ele.tag + ']'
#
#     def GetIcon(self, name):
#         dim = 15
#         try:
#             img = wx.Image(os.path.join('icons', str.lower(name) + '.gif'), wx.BITMAP_TYPE_GIF)
#         except:
#             img = wx.Image(os.path.join('icons', 'invader.gif'), wx.BITMAP_TYPE_GIF)
#         img = img.Scale(dim, dim).ConvertToBitmap()
#         return img
#
#     def Refresh(self):
#
#         page = self.GetCurrentPage()
#
#         self.Clear()
#
# #        self.ParseOrbit(data)
#         self.Populate()
#
#         try: self.SelectPage(page)
#         except: self.SelectPage(dataElementDict.data.tree.get['Name'])
#
#
#     def NameChange(self, target, current, new):
#
#         id = target.get('UID') + 'cat'
#
#         if target.tag not in ['Mission', 'Orbit', 'Spacecraft']:
#             self.SelectProperty(self.GetProperty(id))
#             self.SetPropertyLabel(self.GetProperty(id), new)
#         else:
#             self.GetParent().RefreshAll()
#
#     def SelectFocus(self, target):
#
#         page = target.get('Name')
#         tag = target.tag
#         ele = target
#
#         self.Log.timewrite('Selecting system %s \n' % target.get('Name'))
#
#         while tag != 'Spacecraft':
#             ele = ele.getparent()
#             tag = ele.tag
#             page = ele.get('Name')
#
#         self.SelectPage(page)
#         self.CollapseAll()
#         try:
#             self.Expand(target.get('UID') + '.Category')
#         except:
#             self.Log.timewrite('Could not expand target %s \n' % target.get('Name'))
#
#         #self.SetScrollPos(0, self.GetProperty, 1)
#
#     ### CALLBACKS ###
#
#     def cbParameterChange(self, evt):
#         p = evt.GetProperty()
#         if p:
#
#             ref = p.GetName().encode('ascii')
#             value = p.GetValue()
#             address = ref.split('.')
#
#             # check if current change is parent of last change
#             if ref != self.LastChangedParent:
#
#                 uid = address[0]
#                 name = dataElementDict.data[uid]['Name']
#                 param = address[1]
#
#                 for i in range(2, len(address)):
#                     param = param + '.' + address[i]
#
#                 prev = dataElementDict.data[uid][param]
#                 dataElementDict.data[uid][param] = value
#
#                 # TODO: 3 include units for attribute in the GetValueAsString at end of note
#                 self.Log.timewrite('%s %s changed from %s to %s\n' % (name, param, str(prev), str(value)))
#                 self.LastChanged = ref
#             else:
#                 self.Log.timewrite('Change flowed through to parent\n')
#
#             index = 1+len(address[len(address)-1])
#             self.LastChangedParent = ref[:-index]
#
#
#     def cbPageChange(self, evt):
#         index = self.GetSelectedPage()
#         self.Log.timewrite('Page Changed to \'%s\'\n' % (self.GetPageName(index)))
#
#     def cbParameterSelect(self, evt):
#         p = evt.GetProperty()
#         # TODO : 2 causes non-fatal error so lock functionality removed
#         #lock = not(self.IsPropertyEnabled(p))
#         if p:
#             self.Log.timewrite('%s selected\n' % p.GetName())
#             #self.Log.timewrite('%s selected (Lock set to %s)\n' % (p.GetName(), str(lock)))
#         else:
#             self.Log.timewrite('Nothing selected\n')
#
#     def cbParameterLock(self, evt):
#         """
#         Right clicking on a property allows the parameter to be unlocked
#         """
#         p = evt.GetProperty()
#         n = p.GetName()
#         # FIXME : 1 needs the lock only on selection and against attributes
#         # FIXME : 3 add lock to default and function options
#         if p:
#             self.Log.timewrite('%s right clicked\n' % n)
#             m = wx.Menu()
#             if self.IsPropertyEnabled(p) == True:
#                 self.EnableProperty(p, False)
#                 self.SetPropertyTextColour(p, 'DARK RED')
#                 self.Log.timewrite('%s is currently locked for editing\n' % n)
#                 m.AppendItem(wx.MenuItem(m, id=wx.NewId(), text="Lock parameter to current"))                 ## should be red
#                 m.AppendItem(wx.MenuItem(m, id=wx.NewId(), text="Lock parameter to default", enabled=False))  ## should be green
#                 m.AppendItem(wx.MenuItem(m, id=wx.NewId(), text="Lock parameter to function", enabled=False)) ## should be orange
#             else:
#                 self.EnableProperty(p, True)
#                 # FIXME: 3 PropertyTextColour not changing
#                 self.SetPropertyTextColour(p, 'DARK BLUE')
#                 self.Log.timewrite('%s is currently unlocked for editing\n' % n)
#                 m.AppendItem(wx.MenuItem(m, id=wx.NewId(), text="Unlock parameter"))                           ## should be blue
#             # FIXME : 3 popup at correct location
#             self.PopupMenu(m, (10,10))
#             m.Destroy()
#         else:
#             self.Log.timewrite('Nothing right clicked\n')
