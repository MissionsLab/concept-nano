__author__ = 'SteveG'
import wx

class ChangerWidgetView(wx.Frame):

    def __init__(self, parent):

        wx.Frame.__init__(self, parent, title="Changer Widget")

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.btnAdd = wx.Button(self, label="Add Value")
        self.btnRemove = wx.Button(self, label="Remove Value")
        self.btnMultiply = wx.Button(self, label="Multiply Value")
        self.btnDivide = wx.Button(self, label="Divide Value")
        #self.txtLog = wx.TextCtrl(self, style= wx.TE_MULTILINE, size=(800,100))
        #self.btnUndo = wx.Button(self, label="Undo")

        sizer.Add(self.btnAdd, 0, wx.EXPAND | wx.ALL)
        sizer.Add(self.btnRemove, 0, wx.EXPAND | wx.ALL)
        sizer.Add(self.btnMultiply, 0, wx.EXPAND | wx.ALL)
        sizer.Add(self.btnDivide, 0, wx.EXPAND | wx.ALL)
        #sizer.Add(self.txtLog, 0, wx.EXPAND | wx.ALL)
        #sizer.Add(self.btnUndo, 0, wx.EXPAND | wx.ALL)

        self.SetSizerAndFit(sizer)

    #def addRecord(self, txt):
    #    self.txtLog.WriteText(txt)
if __name__ == '__main__':
    class MyApp(wx.App):
        def OnInit(self):
            frame = ChangerWidgetView(None)
            frame.Show(True)
            return True

    app = MyApp(0)

