__author__ = 'SteveG'

import wx
import wx.propgrid as wxpg

class FrameTree(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent)
        self.pnlTree = TreePanel(self)

class TreePanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.tree = wx.TreeCtrl(self)
        self.szr = wx.BoxSizer(wx.VERTICAL)
        self.szr.Add(self.tree, 0, wx.EXPAND | wx.ALL)
        self.SetSizerAndFit(self.szr)

    def addRoot(self, root):
        item = self.tree.AddRoot(root.name)
        self.tree.SetPyData(item, root.uid)
        return item

    def addChild(self, parent, element):
        item = self.tree.AppendItem(parent, element.name)
        self.tree.SetPyData(item, element.uid)
        return item

    def refresh(self):
        self.Layout()
       # def


class TreeStructure(wx.TreeCtrl):

    def __init__(self, parent, log):

        self.Log = log
        self.p = parent

        wx.TreeCtrl.__init__(self, parent, style=wx.TR_EDIT_LABELS | wx.TR_HAS_BUTTONS)
        # wx.TR_FULL_ROW_HIGHLIGHT | wx.TR_ROW_LINES | wx.TR_SINGLE)
        # prepare icons

        self.SetMinSize((200,200))

        dim = 15
        self.icons = wx.ImageList(dim, dim, True)
        dfn = os.path.join('icons', 'invader.gif')
        for i in range(0, len(equiplist) - 1):
            fn = os.path.join('icons', str.lower(equiplist[i]) + '.gif')
            if os.path.isfile(fn):
                img = wx.Image(fn, wx.BITMAP_TYPE_GIF).Scale(dim, dim)
            else:
                img = wx.Image(dfn, wx.BITMAP_TYPE_GIF).Scale(dim, dim)
            self.icons.Add(img.ConvertToBitmap())

        self.AssignImageList(self.icons)

        #print(data.attrib)

        self.populate()

        self.Bind(wx.EVT_TREE_END_LABEL_EDIT, self.cbNameChange, self)

        self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.cbSelectActive, self)

        self.Bind(wx.EVT_TREE_ITEM_MENU, self.cbSelectOptions, self)

    def populate(self):

        #print(data.get('Name'))
        print(dataElementDict.data.tree.get('Name'))
        self.root = self.AddRoot(dataElementDict.data.tree.get('Name'))
        try:
            self.SetItemImage(self.root, equiplist.index(dataElementDict.data.tree.tag))
        except:
            dataElementDict.data.tree.tag
            print('element not in equipment list')
        self.SetPyData(self.root, dataElementDict.data.tree.attrib)
        #print(self.GetPyData(self.root))

        self.children(self.root, dataElementDict.data.tree.iterchildren())

        self.ExpandAll()

        #self.Expand

    def children(self, parent, child):

        for element in child:

            if element.tag in equiplist:

                data = element.attrib
                data['Eclass'] = element.tag
                #data['uid'] = element

                child = self.AppendItem(parent, element.get('Name'))
                self.SetItemImage(child, equiplist.index(element.tag))
                self.SetPyData(child, data)

                children = element.iterchildren()

                self.children(child, children)

    def refresh(self):

        self.DeleteAllItems()

        self.populate()

    def cbSelectActive(self, evt):

        uid = self.GetPyData(evt.GetItem())['UID']
        target = self.p.pnlEditor.GetElementFromUID(uid)

        #print(target)

        self.p.pnlEditor.SystemEditor.SelectFocus(target)

    def cbNameChange(self, evt):

        uid = self.GetPyData(evt.GetItem())['UID']
        target = self.p.pnlEditor.GetElementFromUID(uid)
        new = evt.GetLabel()

        current = target.get('Name')

        if new != current:
            target.set('Name', new)
            self.p.pnlEditor.NameChange(target, current, new)

    def cbSelectOptions(self, evt):

        try:
            print(self.GetItemText(self.GetSelection()))
        except:
            print('no item selected')
            return

        menu = wx.Menu()

        #movesub = wx.Menu()

        eclass = self.GetItemPyData(evt.GetItem())['Eclass']

        # possible childs
        if eclass == 'Mission':
            child = [ 'Spacecraft', 'Orbit' ]
        elif eclass == 'Spacecraft':
            child = [ 'Federated', 'Integrated', 'Device' ]
        elif eclass == 'Structure':
            child = [ 'Federated', 'Device' ]
        elif eclass == 'Structure':
            child = [ 'Antenna', 'Device' ]
        elif eclass == 'Integrated':
            child = [ 'Module', 'Device' ]
        elif eclass == 'Array':
            child = [ 'Panel' ]
        elif eclass == 'Panel':
            child = [ 'Device', 'Antenna' ]
        elif eclass == 'Module':
            child = [ 'Submodule', 'Antenna', 'Device' ]
        elif eclass == 'Harnessing':
            child = [ 'Harness' ]
        else:
            child = []

        addsub = wx.Menu()

        self.option = {}
        for str in child:
            ch = addsub.AppendItem(wx.MenuItem(addsub, id=-1, text=str))
            self.option[ch.GetId()] = str
            self.Bind(wx.EVT_MENU, self.cbAddElement, ch)

        if child != []:
            add = wx.MenuItem(menu, id=-1, text="Add child ...")
            add.SetSubMenu(addsub)
            menu.AppendItem(add)

        if eclass not in [ 'Mission', 'Structure', 'Array', 'Harnessing' ]:
            clone = wx.MenuItem(menu, id=-1, text="Clone")
            menu.AppendItem(clone)
            self.Bind(wx.EVT_MENU, self.cbCloneElement, clone)

        if eclass not in [ 'Mission', 'Structure', 'Array', 'Harnessing' ]:
            remove = wx.MenuItem(menu, id=-1, text="Remove")
            menu.AppendItem(remove)
            self.Bind(wx.EVT_MENU, self.cbRemoveElement, remove)

        self.PopupMenu(menu, evt.GetPoint())

        menu.Destroy()

    def cbCloneElement(self, evt):

        uid = self.GetPyData(self.GetSelection())['UID']

        target = self.GetParent().GetElementFromUID(uid)

        self.GetParent().CopyElement(target)

    def cbRemoveElement(self, evt):

        sel = self.GetSelection()

        uid = self.GetPyData(sel)['UID']

        target = self.GetParent().GetElementFromUID(uid)

        self.GetParent().RemoveElement(target)

    def cbAddElement(self, evt):

        id = evt.GetId()

        text = self.option[id]

        uid = self.GetPyData(self.GetSelection())['UID']

        parent = self.p.pnlEditor.GetElementFromUID(uid)

        self.p.pnlEditor.AddElement(parent, text)