__author__ = 'SteveG'

import wx
import wx.aui

from root.gui import guiTree
from root.gui import guiEditor
from root.gui import guiModel3DView
from root.gui import guiLogView

class MainFrame(wx.Frame):

    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title)

        self.menu = wx.MenuBar()
        self.menu.SetName('Main')
        self.SetMenuBar(self.menu)

        self.status = self.CreateStatusBar()

        self.mgrWorkspace = wx.aui.AuiManager()
        self.mgrWorkspace.SetManagedWindow(self)

        self.nbkWorkspace = wx.aui.AuiNotebook(self)
        self.nbkWorkspace.ToggleWindowStyle(wx.aui.AUI_NB_CLOSE_ON_ACTIVE_TAB)

        self.pnlOptions = wx.Panel(self)
        self.pnlTree = guiTree.TreePanel(self)
        self.pnlEditor = guiEditor.EditorPanel(self)
        self.pnlConfiguration = guiModel3DView.Model3DPanel(self)
        self.pnlLog = guiLogView.LogView(self)

        sz = self.GetClientSize()

        self.mgrWorkspace.AddPane(self.pnlOptions, wx.aui.AuiPaneInfo().
            Top().CloseButton(False).Caption('Options').CaptionVisible(False).
            BestSize(wx.Size(sz[0], 50)))

        self.mgrWorkspace.AddPane(self.pnlTree, wx.aui.AuiPaneInfo().
            Left().CloseButton(False).Caption('Tree').CaptionVisible(True).
            BestSize(wx.Size(max(300, sz[0]*0.25), sz[1])))

        self.mgrWorkspace.AddPane(self.pnlEditor, wx.aui.AuiPaneInfo().
            Right().CloseButton(False).Caption('Editor').CaptionVisible(True).
            BestSize(wx.Size(max(300, sz[0]*0.25), sz[1])))

        self.mgrWorkspace.AddPane(self.nbkWorkspace, wx.aui.AuiPaneInfo().
            CentrePane().CloseButton(False).Caption('Workspace').CaptionVisible(True).
            BestSize(wx.Size(sz[0]*0.6, sz[1]-250)))

        self.mgrWorkspace.AddPane(self.pnlLog, wx.aui.AuiPaneInfo().
            Bottom().CloseButton(False).Caption('Log').CaptionVisible(True).
            BestSize(wx.Size(sz[0], 200)))

        self.nbkWorkspace.AddPage(self.pnlConfiguration, "Configuration")
        self.pnlConfigurationIndex = self.nbkWorkspace.GetPageIndex(self.pnlConfiguration)

        # WINDOW CONFIGURATION
        #  _ ___ ___ _________
        # |1|2  |3  |4        |
        # | |   |   |         |
        # | |   |   |         |
        # | |___|___|_________|
        # |_|5_____________|6_|
        # |___
        # 1 Options, 2 Tree, 3 Editor, 4 Workspace, 5 Log, 6 Logo

        self.mgrWorkspace.Update()
