
class BaseController (object):
    def __init__(self, model=None, parent=None, children=[]):
        self.parent = parent
        self.children = children

        #if parent is not None:
        #    self.parent.children.append(self)

        if model is None:
            self.instantiateModel()
        else:
            self.model = model

    def instantiateModel (self):
        raise NotImplementedError('You have to implement this method in the base class')