from root.ctl import ctlBaseController
import wx

class LogController (ctlBaseController.BaseController):
    def __init__(self, model=None, parent=None):
        super(LogController, self).__init__(model, parent)

        self.parent.guiLog.btnSave.Bind(wx.EVT_BUTTON, self.cbSave)
        self.parent.guiLog.btnLoad.Bind(wx.EVT_BUTTON, self.cbLoad)

    def cbUndo(self, evt):
        self.model.log.undo()

    def cbSave(self, evt):
        self.model.log.save()

    def cbLoad(self, evt):
        self.model.log.load(fn='data/record.log')
        self.parent.guiLog.clearRecord()
        for r in self.model.log.records:
            self.parent.guiLog.addRecord(''.join((str(r), '\n')))

    def cbSelectVar(self, evt):
        self.model.selectVariable()