__author__ = 'SteveG'


from pubsub import pub

from root.ctl import ctlBaseController

class TreeController (ctlBaseController.BaseController):

    def __init__(self, model=None, parent = None, children = []):
        super(TreeController, self).__init__(model, parent=parent, children=children)

        # alternatively to subscribe to all, just remove second argument
        pub.subscribe(self.pbPopulate, "root.mdl.tree.Tree.actPopulate")

    def pbPopulate(self, arg=None):
        #data = arg
        root = self.model.dataElement('0')
        item = self.parent.guiTree.addRoot(root)
        self.model.dataElement('0').treeitem = item
        self.iterChildren(root, item)

    # recursive iteration to populate children in tree
    def iterChildren(self, parent, parentitem):
        for c in parent.children:
            child = self.model.dataElement(c)
            childitem = self.parent.guiTree.addChild(parentitem, child)
            self.model.dataElement(child.uid).treeitem = childitem
            self.iterChildren(child, childitem)
