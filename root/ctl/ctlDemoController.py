## libraries
import wx
from pubsub import pub

## base
from root.ctl import ctlBaseController

## model
from root.mdl.example import mdlMainModel
from root.mdl.data import mdlData
from root.mdl.timer import mdlTimer

## control
from root.ctl import ctlTransform
from root.ctl import ctlLog
from root.ctl import ctlPrint
from root.ctl import ctl3DModel
from root.ctl import ctlEditor
from root.ctl import ctlTree

## view
from root.gui import guiDataView
from root.gui import guiChangerWidget
from root.gui import guiLogView
from root.gui import guiModel3DView
from root.gui import guiEditor
from root.gui import guiTree

class DemoController (ctlBaseController.BaseController):

    def __init__(self, model=None):

        super(DemoController, self).__init__(model, parent = None, children = [])

        # low level print controller initiate asap for output
        self.ctlPrint = ctlPrint.PrintController(self.model)

        # frame and views
        self.frame = wx.Frame(None, -1)
        self.view1 = guiDataView.DataView(self.frame)
        self.view2 = guiChangerWidget.ChangerWidgetView(self.frame)
        self.guiLog = guiLogView.LogView(self.frame)
        self.gui3DModel = guiModel3DView.Model3DView(self.frame)
        self.guiEditor = guiEditor.FrameEditor(self.frame)
        self.guiTree = guiTree.FrameTree(self.frame)

        # TODO -- check is datamodel used
        self.datamodel = mdlData.Data(self.model.file)
        self.timer = mdlTimer.Timer(self)
        self.timer.go()

        self.ctlTransform = ctlTransform.TransfromController(model1=self.model.model1, model2=self.model.model2, parent = self)
        self.ctlLog = ctlLog.LogController(self.model, parent=self)
        self.ctl3DModel = ctl3DModel.Model3DController(self.model, parent=self)
        self.ctlEditor = ctlEditor.EditorController(self.model, parent=self)
        self.ctlTree = ctlTree.TreeController(self.model, parent=self)

        uid = self.model.data.keys()
        i = 0

        # quick hack for demonstrating the dataElement
        for uid in ['1', '2', '3', '4', '5']:
            i =+ 1
            value = self.model.dataElement(uid, 'value').value
            name = self.model.dataElement(uid).name
            self.view1.setLabel(i, name)
            self.view1.setValue(i, value)

        pub.subscribe(self.pbValueChanged, "root.data.value.set")
        pub.subscribe(self.pbActionLog, "root.log.record")
        pub.subscribe(self.pbTimerCycle, "root.timer.majorCycle")

        self.guiLog.btnUndo.Bind(wx.EVT_BUTTON, self.ctlLog.cbUndo)

        self.model.tree.actPopulate()
        self.model.editor.actPopulate()
        self.model.visual.actPopulate()

        # show all views at the end
        self.view1.Show()
        self.view2.Show()
        self.guiLog.Show()
        self.gui3DModel.Show()
        self.guiEditor.Show()
        self.guiTree.Show()



    def instantiateModel (self):
        self.model = mdlMainModel.MainModel()

    def pbValueChanged (self, arg=None):
        self.view1.setValue(0, arg)

    def pbActionLog(self, arg=None):
        self.guiLog.addRecord(arg)

    def pbTimerCycle(self, arg=None):
        self.view1.setPeriodic(arg)
        self.gui3DModel.panel.actRerender()