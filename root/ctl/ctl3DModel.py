from pubsub import pub

from root.ctl import ctlBaseController

class Model3DController (ctlBaseController.BaseController):

    def __init__(self, model=None, parent = None, children = []):
        super(Model3DController, self).__init__(model, parent=parent, children=children)

        # alternatively to subscribe to all, just remove second argument
        pub.subscribe(self.pb3DPopulate, "root.3DModel.actPopulate")
        pub.subscribe(self.pbRerender, "root.data.model.update")

    def pb3DPopulate(self, arg=None):
        self.parent.guiModel3D.actPopulate(arg)

    def pbRerender(self):
        self.parent.guiModel3D.actRerender()



