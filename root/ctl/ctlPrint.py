from pubsub import pub

from root.ctl import ctlBaseController


class PrintController (ctlBaseController.BaseController):

    def __init__(self, model=None, parent = None, children = []):
        super(PrintController, self).__init__(model, parent=parent, children=children)

        # alternatively to subscribe to all, just remove second argument
        pub.subscribe(self.pbPrint, "root")
        pub.subscribe(self.pbPrint, "warn")

    def pbPrint(self, arg=None, msg='none', topic=pub.AUTO_TOPIC):
        pmsg = ("%s : %s ( %s )" % (topic.getName(), msg, arg))
        self.model.log.record('debug', msg=pmsg)
        print(pmsg)