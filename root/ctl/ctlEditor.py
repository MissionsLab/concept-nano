__author__ = 'SteveG'

from pubsub import pub
import wx.propgrid as wxpg

from root.ctl import ctlBaseController

class EditorController (ctlBaseController.BaseController):

    def __init__(self, model=None, parent = None, children = []):
        super(EditorController, self).__init__(model, parent=parent, children=children)

        self.lastChangedParent = None

        # alternatively to subscribe to all, just remove second argument
        pub.subscribe(self.pbAddElementToEditor, "root.mdl.data.Data.addElement")
        pub.subscribe(self.pbPopulate, "root.mdl.editor.Editor.actPopulate")

        self.parent.guiEditor.Bind(wxpg.EVT_PG_CHANGED, self.cbParameterChange)

    ### FLOW FROM GUI

    def cbParameterChange(self, evt):
        prop = evt.GetProperty()
        if prop:
            ref = prop.GetName().encode('ascii')
            value = prop.GetValue()
            address = ref.split('.')
            if ref != self.lastChangedParent:
                uid = address[0]
                name = self.model.dataElement(uid).name
                param = address[1]
                for i in range(2, len(address)):
                     param = param + '.' + address[i]
                prev = self.model.dataElement(uid, param).value
                self.model.dataElement(uid, param).value = value

    ### FLOW TO GUI

    def pbPopulate(self, arg=None, msg=None):
        #data = arg
        root = self.model.dataElement('0')
        page = self.parent.guiEditor.addPage(root.name)
        self.iterChildren(root)
        self.parent.guiEditor.addPage('test')

    # recursive iteration to populate children in tree
    def iterChildren(self, parent):
        for c in parent.children:
            child = self.model.dataElement(c)
            self.addElement(child)
            self.iterChildren(child)

    def pbAddElementToEditor(self, arg=None, topic=pub.AUTO_TOPIC):
        self.addElement(arg)

    def addElement(self, ele):
        print('add element %s %s' % (ele.name, ele.uid))
        self.parent.guiEditor.addCategory(''.join(('', ele.uid)), ele.name)
        for attrib in ele.attribute.keys():
            ref = ''.join(('', ele.uid, '.', attrib))
            value = ele.attribute[attrib].value
            editor = ele.attribute[attrib].editor
            lock = ele.attribute[attrib].lock
            self.parent.guiEditor.addProperty(ref, attrib, value, editor, lock)
        self.parent.guiEditor.refresh()
        pub.sendMessage('root.ctl.ctlEditor.addElement')
